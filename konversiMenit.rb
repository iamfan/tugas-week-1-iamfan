def minuteConversion(minute)
  hour = minute / 60;
  tmpMinute = minute % 60;
  tmpMinute = "0#{tmpMinute}" if tmpMinute < 10
  "#{hour}:#{tmpMinute}"  
end
  
puts minuteConversion(63) # 1:03
puts minuteConversion(124) # 2:04
puts minuteConversion(53) # 0:53
puts minuteConversion(88) # 1:28
puts minuteConversion(120) # 2:00