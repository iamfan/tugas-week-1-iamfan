def bandingkanAngka(a1, a2)
  return -1 if a1 == a2
  a1 < a2
end


puts bandingkanAngka(5, 8) # true
puts bandingkanAngka(5, 3) # false
puts bandingkanAngka(4, 4) # -1
puts bandingkanAngka(3, 3) # -1
puts bandingkanAngka(17, 2) # false
